@echo off

set EJECUTABLE="..\bin\tp0.exe"

@ECHO.
echo FIUBA - Algoritos y Programacion II - TP 0
@ECHO.
echo AUTOR: Roberto Cabellon - Leg. 98969
@ECHO.
@ECHO.
echo -----------------------------------------
@ECHO.
@ECHO.
if not exist %EJECUTABLE% (
	echo Compilar el archivo main.c y ubicar el ejecutable tp0.exe en la carpeta bin\
	@ECHO.
	@ECHO.
	echo -----------------------------------------
	@ECHO.
	@ECHO.
	goto exit
)


echo Presionar una tecla para iniciar las pruebas ...
pause >nul

copy /y reg-bak.txt reg.txt


rem ==========================================
cls


@ECHO.
echo Consultar un regsitro existente
@ECHO.
echo tp0.exe -c reg.txt 34991645
@ECHO.
echo Presionar una tecla para ejecutar ...
pause >nul
@ECHO.
echo -----------------------------------------
@ECHO.
@ECHO.
%EJECUTABLE% -c reg.txt 34991645
@ECHO.
@ECHO.
echo -----------------------------------------
@ECHO.
echo Presionar una tecla para continuar ...
pause >nul


rem ==========================================
cls


@ECHO.
echo Consultar un regsitro inexistente
@ECHO.
echo tp0.exe -c reg.txt 33456789
@ECHO.
echo Presionar enter para ejecutar ...
pause >nul
@ECHO.
echo -----------------------------------------
@ECHO.
@ECHO.
%EJECUTABLE% -c reg.txt 33456789
@ECHO.
@ECHO.
echo -----------------------------------------
@ECHO.
@ECHO.
echo Presionar una tecla para continuar ...
pause >nul


rem ==========================================
cls


@ECHO.
echo Dar de alta un nuevo regsitro
@ECHO.
echo bin\tp0.exe -a reg.txt 33456789 romulo remo 33 234234 romulo@remo.net
@ECHO.
echo Presionar enter para ejecutar ...
pause >nul
@ECHO.
echo -----------------------------------------
@ECHO.
@ECHO.
%EJECUTABLE% -a reg.txt 33456789 romulo remo 33 234234 romulo@remo.net
@ECHO.
@ECHO.
echo -----------------------------------------
@ECHO.
echo Presionar una tecla para continuar ...
pause >nul


rem ==========================================
cls


@ECHO.
echo Consultar el regsitro agregado
@ECHO.
echo bin\tp0.exe -c reg.txt 33456789
@ECHO.
echo Presionar enter para ejecutar ...
pause >nul
@ECHO.
echo -----------------------------------------
@ECHO.
@ECHO.
%EJECUTABLE% -c reg.txt 33456789
@ECHO.
@ECHO.
echo -----------------------------------------
@ECHO.
echo Presionar una tecla para continuar ...
pause >nul


rem ==========================================
cls



@ECHO.
@ECHO.
echo Fin de las Pruebas
@ECHO. 
@ECHO.
echo -----------------------------------------
@ECHO.


:exit
echo Presionar una tecla para finalizar
pause >nul
