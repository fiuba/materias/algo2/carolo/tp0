#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int dni;
    int telefono;
    int edad;
    char apellido[50];
    char nombre[50];
    char mail[50];
} TReg;

// precondicion: argumentos validos
void parse_args (const int argc, const char** argv, char* opt, char* archivo, TReg* reg);

// precondicion: archivo con formato valido
int buscar_registro(const char* archivo, TReg* reg);

//precondicion: registro cargado
void mostrar_registro(TReg* reg);


int main (int argc, char** argv)
{
    char opt;
    char archivo[100];
    TReg reg;

    parse_args(argc, argv, &opt, archivo, &reg);

 	switch(opt)
    {
      case 'c' :
        switch (buscar_registro(archivo, &reg))
        {
            case 0:
                mostrar_registro(&reg);
                break;
            case 1:
                printf("ERROR. El archivo no existe.\n");
                break;
            case 2:
                printf("No se encontraron reguistros con DNI: %d\n", reg.dni);
                break;
        }
        break;
      case 'a' :
        switch (grabar_registro(archivo, &reg))
        {
            case 0:
                printf("Registro grabado satisfactoriamente.\n");
                break;
            case 1:
                printf("ERROR. No se pudo abrir el archivo.\n");
                break;
            case 2:
                printf("ERROR. El registro no ha sido grabado.\n");
                break;
        }
        break;
      default :
        printf("ERROR. argumento no reconocido.\n");
    }

    return 0;
}

void parse_args (const int argc, const char** argv, char* opt, char* archivo, TReg* reg)
{
    char** arg = argv + 1;

    *opt = *( (*arg++) + 1 );

    strcpy(archivo, *arg++);

    reg -> dni = atol(*arg++);

    if (argc > 4)
    {
        strcpy(reg->apellido, *arg++);
        strcpy(reg->nombre, *arg++);
        reg->edad = atoi(*arg++);
        reg->telefono = atol(*arg++);
        strcpy(reg->mail, *arg++);
    }

}

int buscar_registro(const char* archivo, TReg* reg)
{
    int res = 0;
    int found = 0;
    FILE* f;
    TReg aux;

    if ((f = fopen(archivo, "r")) == NULL)
    {
        res = 1; //archivo no encontrado
    }
    else
    {
        while ( !feof(f) && !found )
        {
            fscanf(f, "%d,%[^,],%[^,],%d,%d,%[^\n]",
                   &aux.dni,
                   aux.apellido,
                   aux.nombre,
                   &aux.edad,
                   &aux.telefono,
                   aux.mail
                   );

            found = aux.dni == reg->dni;
        }

        if (found)
            *(reg) = aux;
        else
            res = 2; // registro no encontrado

    }
    fclose(f);
    return res;

}

void mostrar_registro(TReg* reg)
{
    printf("DNI:      %d\nAPELLIDO: %s\nNOMBRE:   %s\nEDAD:     %d\nTELEFONO: %d\nMAIL:     %s\n",
            reg->dni,
            reg->apellido,
            reg->nombre,
            reg->edad,
            reg->telefono,
            reg->mail
            );
}

int grabar_registro(char* archivo, TReg* reg)
{
    FILE* f;
    int res;

    if ( (f = fopen(archivo, "a")) == NULL )
    {
        res = 1;
    }
    else if (
        fprintf(f,"%d,%s,%s,%d,%d,%s\n",
            reg->dni,
            reg->apellido,
            reg->nombre,
            reg->edad,
            reg->telefono,
            reg->mail) )
    {
        res = 0;
    }
    else
    {
        res = 2;
    }
    fclose(f);
    return res;
}
